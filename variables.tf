variable "env" {
  description = "Please enter environment"
  default     = "dev"
}

variable "region" {
  description = "Please enter AWS region to deploy server"
  default     = "us-east-1"
}

variable "name_profile" {
  description = "Please enter name of your profile"
  default     = "iceforest"
}

variable "name_dynamic_security_group" {
  default = "Dynamic Security Group"
}

variable "allow_ports" {
  description = "list of ports to open server"
  type        = list(any)
  default     = ["80", "22", "81", "3000", "9090", "9093", "9094", "9100", "24224", "3100", "9080"]
}

variable "cidr_blocks_ingress" {
  type    = list(any)
  default = ["0.0.0.0/0"]
}

variable "cidr_blocks_egress" {
  type    = list(any)
  default = ["0.0.0.0/0"]
}

variable "tag_name_aws_security_group" {
  default = "Web access for Application"
}

variable "name_prefix_launch_configuration" {
  default = "Web-server-"
}

variable "instance_type" {
  description = "Please enter instance type"
  default     = "t2.micro"
}

variable "ssh_key_name" {
  description = "Please enter your ssh key name"
  default     = "iceforest"
}


variable "health_check_type" {
  default = "ELB"
}

variable "name_autoscaling_group" {
  default = "WebServer in Auto Scalling Group"
}

variable "min_size_instance" {
  type    = number
  default = 2
}

variable "max_size_instance" {
  type    = number
  default = 2
}

variable "min_elb_capacity" {
  type    = number
  default = 2
}

variable "lb_port" {
  type    = number
  default = 80
}

variable "instance_port" {
  type    = number
  default = 80
}
variable "healthy_threshold" {
  type    = number
  default = 2
}

variable "unhealthy_threshold" {
  type    = number
  default = 10
}

variable "timeout" {
  type    = number
  default = 60
}

variable "interval" {
  type    = number
  default = 70
}

variable "name_elastic_load_balancer" {
  default = "ELB"
}
